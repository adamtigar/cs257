# Adam Tigar
# CS 257: Software Design
# April 15, 2016

# Returns city and state of the zipcode entered. Same code as 
# api_test.py. 

import sys
import argparse
import json
import urllib.request


# Fetches the city given a zipcode and a coutnry
def get_city_plus(country, zipcode):
	base_url = 'http://api.zippopotam.us/{0}/{1}'
	url = base_url.format(country, zipcode)

	try:
		data_from_server = urllib.request.urlopen(url).read()
		string_from_server = data_from_server.decode('utf-8')
		location_list = json.loads(string_from_server)

	except Exception as e:
		return []
	full_result = []

	#Searches through the JSON string for items
	try:
		postal = location_list['post code']
		country = location_list['country']
		places = location_list['places']
		places = places[0]
		city = places['place name']
		state = places['state']
		full_result.append({'country': country, 'zip code': postal, 'city': city, 'state': state})
	except Exception as e:
		pass
	return full_result


def main(args):
	if args.action == 'zipcode':
		full_location = get_city_plus(args.country, args.zip)
		for item in full_location:
			zipcode = item['zip code']
			country = item['country']
			city = item['city']
			state = item['state']
		print('The zip code {0} in {1} is in {2}, {3}'.format(zipcode, country, city, state))


if __name__ == '__main__':
	
	zipParser = argparse.ArgumentParser(description='Get location information from the zippopotam.us API')
	

	zipParser.add_argument('action',
						metavar='action',
						help='Confirm you are looking up a zipcode',
						choices=['zipcode'])

	zipParser.add_argument('country',
						metavar='country',
						help='Country is a 2-letter code. See the full list at http://www.zippopotam.us/')

	zipParser.add_argument('zip', help='the zip code you want to lookup')

	zipargs = zipParser.parse_args()

	main(zipargs)