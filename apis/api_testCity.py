# Adam Tigar
# CS 257: Software Design
# April 15, 2016

# Allows for seraching using country, state, and city
# to return the correct zip code.

import sys
import argparse
import json
import urllib.request


#Fetch the zip codes for a city. 

def get_zipcode(country, state, city):
	base_url = 'http://api.zippopotam.us/{0}/{1}/{2}'
	url = base_url.format(country, state, city)

	# fetch JSON file from server
	try:
		data_from_server = urllib.request.urlopen(url).read()
		string_from_server = data_from_server.decode('utf-8')
		location_list = json.loads(string_from_server)
	except Exception as e:
		return []

	full_result = []
	ziplist = []

	# Parse JSON string
	try:
		country = location_list['country']
		places = location_list['places']
		state = location_list['state']
		city = location_list['place name']
		for item in places:
			zippy = item['post code']
			ziplist.append(zippy)
		postal = ziplist
		full_result.append({'country': country, 'zip code': postal, 'city': city, 'state': state})
	except Exception as e:
		pass
	return full_result




def main(args):
	if args.action == 'city':
			full_location = get_zipcode(args.country, args.state, args.city)
			for item in full_location:
				zipcode = item['zip code']
				country = item['country']
				city = item['city']
				state = item['state']
				if len(zipcode) == 1:
					print('The city {0} in {1}, {2} has zipcode {3}'.format(city, state, country, zipcode))
				else:
					print('The city {0} in {1}, {2} has zipcodes {3}'.format(city, state, country, zipcode))
				if len(zipcode) > 5:
					print('Thats a lot!')

# Parse the command line for arguments

if __name__ == '__main__':

	cityParser = argparse.ArgumentParser(description='Get location information from the zippopotam.us API')

	cityParser.add_argument('action',
						metavar='action',
						help='Confirm you are looking for city',
						choices=['city'])

	cityParser.add_argument('country',
						metavar='country',
						help='Country is a 2-letter code. See the full list at http://www.zippopotam.us/',
						choices=['us', 'ca'])

	cityParser.add_argument('state', help='the state of the city')

	cityParser.add_argument('city', help='The city for lookup')

	cityargs = cityParser.parse_args()

	main(cityargs)

